package iOS_tests.forgot_password;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.common_pages.LoginPage;
import pages.common_pages.MainPage;
import pages.iOS.ForgotPasswordPageIOS;
import pages.iOS.TouchIdPopup;
import iOS_tests.IOSTest;

import static org.testng.Assert.assertFalse;

@Feature("Forgot Password")
public class ForgotPasswordViaWrongEmailTest extends IOSTest {

    private MainPage mainPage;
    private LoginPage loginPage;
    private ForgotPasswordPageIOS forgotPasswordPage;
    private TouchIdPopup touchIdPopup;
    private String invalidEmail;

    @BeforeTest
    public void setTestData() {
        invalidEmail = "appstage@sfsd.d";
        mainPage = new MainPage();
        loginPage = new LoginPage();
        forgotPasswordPage = new ForgotPasswordPageIOS();
        touchIdPopup = new TouchIdPopup();
    }

    @Story("Input invalid email to forgot password pop up")
    @Test
    public void inputInvalidEmailTest() {
        mainPage.openLoginPage();
        touchIdPopup.closeTouchIdPopup();
        loginPage.clickForgotPassword();
        forgotPasswordPage.enterEmail(invalidEmail);

        assertFalse(forgotPasswordPage.isButtonRenewMyPasswordEnabled(), "Button 'RenewMyPassword' is not disabled after entering the invalid email");
    }
}
