package iOS_tests.forgot_password;

import core.configs.MainConfig;
import core.imap.ImapClient;
import core.imap.MessageContent;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.common_pages.LoginPage;
import pages.common_pages.MainPage;
import pages.iOS.EmailVerificationPopup;
import pages.iOS.ForgotPasswordPageIOS;
import pages.iOS.TouchIdPopup;
import iOS_tests.IOSTest;

import javax.mail.MessagingException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Forgot Password")
public class ForgotPasswordViaExistEmailTest extends IOSTest {

    private MainPage mainPage;
    private LoginPage loginPage;
    private ForgotPasswordPageIOS forgotPasswordPage;
    private TouchIdPopup touchIdPopup;
    private EmailVerificationPopup emailVerificationPopup;
    private ImapClient imap;
    private String subject = "Password Reset Link";
    private String existsEmail;

    @BeforeTest
    public void setTestData() {
        existsEmail = "appstage+1567586825347@gmail.com";
        mainPage = new MainPage();
        loginPage = new LoginPage();
        forgotPasswordPage = new ForgotPasswordPageIOS();
        touchIdPopup = new TouchIdPopup();
        emailVerificationPopup = new EmailVerificationPopup();
        imap = new ImapClient(existsEmail, MainConfig.user.password());
        imap.deleteAllMessagesWithSubject(subject);
        imap.setMessagesSearchTime(10);
    }

    @Story("Input existed email to forgot password pop up")
    @Test
    public void inputExistEmailTest() throws MessagingException {
        mainPage.openLoginPage();
        touchIdPopup.closeTouchIdPopup();
        loginPage.clickForgotPassword();
        forgotPasswordPage.enterEmail(existsEmail);
        forgotPasswordPage.clickRenewMyPassword();
        emailVerificationPopup.clickOk();

        MessageContent content = imap.getLastMessageWithSubject(subject);
        String resetPasswordLink = content.getLink(2); // fetch 2nd ling from mailbox message

        assertTrue(forgotPasswordPage.isOpened(), "forgotPasswordPage is not opened after entering the exist email to ForgotPassword pop up");
        assertTrue(resetPasswordLink.contains("reset-password"), "Reset password link is not present in email message");
        assertEquals(content.getRecipientEmail(), existsEmail, "Reset password link is not delivered to user");
    }
}
