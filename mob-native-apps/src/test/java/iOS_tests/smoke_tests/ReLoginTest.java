package iOS_tests.smoke_tests;

import core.configs.MainConfig;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.common_pages.LoginPage;
import pages.common_pages.MainPage;
import pages.iOS.DocumentsPageIOS;
import pages.iOS.HeaderPanelIOS;
import pages.iOS.LeftSideBarIOS;
import pages.iOS.TouchIdPopup;
import iOS_tests.IOSTest;

@Feature("ReLogin")
public class ReLoginTest extends IOSTest {

    private MainPage mainPage;
    private LoginPage loginPage;
    private DocumentsPageIOS documentsPage;
    private HeaderPanelIOS headerPanel;
    private LeftSideBarIOS leftSideBar;
    private TouchIdPopup touchIdPopup;
    private String email;

    @BeforeTest
    public void setTestData() {
        email = "appstage+1567586825347@gmail.com";
        mainPage = new MainPage();
        loginPage = new LoginPage();
        documentsPage = new DocumentsPageIOS();
        headerPanel = new HeaderPanelIOS();
        leftSideBar = new LeftSideBarIOS();
        touchIdPopup = new TouchIdPopup();
    }

    @Story("User can login after logout")
    @Test
    public void reloginTest() {
        mainPage.openLoginPage();
        touchIdPopup.closeTouchIdPopup();
        loginPage.login(email, MainConfig.user.password());
        headerPanel.openLeftSideBar();
        leftSideBar.logout();
        mainPage.openLoginPage();
        touchIdPopup.closeTouchIdPopup();
        loginPage.login(email, MainConfig.user.password());

        documentsPage.checkUpgradeButtonIsPresent();
        documentsPage.checkEmptyFolderIsPresent();
    }
}
