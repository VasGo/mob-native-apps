package iOS_tests.smoke_tests;

import core.configs.MainConfig;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.android.DocumentsPageAndroid;
import pages.common_pages.LoginPage;
import pages.common_pages.MainPage;
import pages.iOS.TouchIdPopup;
import iOS_tests.IOSTest;

@Feature("Sign In")
public class LoginByExistedTest extends IOSTest {

    private MainPage mainPage;
    private LoginPage loginPage;
    private DocumentsPageAndroid documentsPage;
    private TouchIdPopup touchIdPopup;
    private String email;

    @BeforeClass
    public void setTestData() {
        email = "appstage+1567586825347@gmail.com";
        mainPage = new MainPage();
        loginPage = new LoginPage();
        documentsPage = new DocumentsPageAndroid();
        touchIdPopup = new TouchIdPopup();
    }

    @Story("Login by existed user")
    @Test
    public void signInTest() {
        mainPage.openLoginPage();
        touchIdPopup.closeTouchIdPopup();
        loginPage.login(email, MainConfig.user.password());

        documentsPage.checkUpgradeButtonIsPresent();
        documentsPage.checkEmptyFolderIsPresent();
    }
}
