package iOS_tests.smoke_tests;

import core.configs.MainConfig;
import core.imap.ImapClient;
import core.imap.MessageContent;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.common_pages.MainPage;
import pages.common_pages.RegistrationPage;
import pages.iOS.DocumentsPageIOS;
import pages.iOS.EmailVerificationPopup;
import pages.web_pages.ConfirmationPageIOS;
import iOS_tests.IOSTest;
import utils.StringTool;

import java.io.IOException;

import static core.TLDriverFactory.getAppDriver;
import static org.testng.Assert.assertTrue;

@Feature("Registration")
public class RegistrationTest extends IOSTest {

    private MainPage mainPage;
    private RegistrationPage registrationPage;
    private ConfirmationPageIOS confirmationPage;
    private EmailVerificationPopup emailVerificationPopup;
    private DocumentsPageIOS documentsPage;
    private String email;
    private ImapClient imap;
    private String subject = "Email Verification.";

    @BeforeClass
    public void setTestData() {
        email = StringTool.makeUniqueEmail(MainConfig.user.email());
        imap = new ImapClient(email, MainConfig.user.password());
        imap.deleteAllMessagesWithSubject(subject);
        imap.setMessagesSearchTime(10);
        mainPage = new MainPage();
        registrationPage = new RegistrationPage();
        confirmationPage = new ConfirmationPageIOS();
        documentsPage = new DocumentsPageIOS();
        emailVerificationPopup = new EmailVerificationPopup();
    }

    @Story("Register new user and confirm registration")
    @Test
    public void registrationTest() throws IOException {
        mainPage.openSignUpPage();
        registrationPage.registerUser(email, MainConfig.user.password());
        assertTrue(emailVerificationPopup.getTextFromEmailVerification().contains(email), "Wrong registered email in verification pop up");
        emailVerificationPopup.clickOk();

        MessageContent content = imap.getLastMessageWithSubject(subject);
        String verificationLink = content.getLink(2); // fetch 2nd ling from mailbox message

        mobPlaform.launchBrowserMobile();
        mobPlaform.openUrl(verificationLink);
        confirmationPage.clickIhaveTheApp();
        getAppDriver().context("NATIVE_APP");

        documentsPage.checkUpgradeButtonIsPresent();
        documentsPage.checkEmptyFolderIsPresent();
    }
}
