package iOS_tests.smoke_tests;

import core.configs.MainConfig;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.common_pages.LoginPage;
import pages.common_pages.MainPage;
import pages.common_pages.SupportPage;
import pages.common_pages.popups.ThankYouPopup;
import pages.enums.Help;
import pages.enums.LeftSideBarPage;
import pages.iOS.HeaderPanelIOS;
import pages.iOS.HelpPopupIOS;
import pages.iOS.LeftSideBarIOS;
import pages.iOS.TouchIdPopup;
import iOS_tests.IOSTest;

import static org.testng.Assert.*;

@Feature("Contact Support")
public class ContactSupportTest extends IOSTest {

    private MainPage mainPage;
    private LoginPage loginPage;
    private HeaderPanelIOS headerPanel;
    private HelpPopupIOS helpPopup;
    private SupportPage supportPage;
    private ThankYouPopup thankYouPopup;
    private TouchIdPopup touchIdPopup;
    private LeftSideBarIOS leftSideBar;
    private String email;
    private String supportText;

    @BeforeTest
    public void setTestData() {
        email = "appstage+1567586825347@gmail.com";
        supportText = "Test";
        mainPage = new MainPage();
        loginPage = new LoginPage();
        headerPanel = new HeaderPanelIOS();
        helpPopup = new HelpPopupIOS();
        supportPage = new SupportPage();
        thankYouPopup = new ThankYouPopup();
        touchIdPopup = new TouchIdPopup();
        leftSideBar = new LeftSideBarIOS();
    }

    @Story("Send message to support")
    @Test
    public void sendMessageToSupportTest() {
        mainPage.openLoginPage();
        touchIdPopup.closeTouchIdPopup();
        loginPage.login(email, MainConfig.user.password());
        headerPanel.openLeftSideBar();
        leftSideBar.selectPageFromLeftSideBar(LeftSideBarPage.HELP);
        helpPopup.selectHelpAction(Help.SUPPORT);
        supportPage
                .inputSubject(supportText)
                .inputBody(supportText)
                .sendMessageToSupport();

        assertEquals(thankYouPopup.getTitle(), "Thank You!", "Wrong title in thank you pop up");
        assertEquals(thankYouPopup.getMessage(), "Your message has been sent.", "Wrong message in thank you pop up");

        thankYouPopup.clickOk();
        assertTrue(leftSideBar.isOpened(), "Left sidebar is not opened after clicking OK in 'Thank You pop up'");
        assertFalse(thankYouPopup.isPopupDisplayed(), "'Thank You pop up' is stayed after clicking on button 'OK'");
    }
}
