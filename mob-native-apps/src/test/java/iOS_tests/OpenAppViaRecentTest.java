package iOS_tests;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.common_pages.LoginPage;
import pages.common_pages.MainPage;
import utils.IOSTool;

import static org.testng.Assert.assertTrue;

@Feature("Recent apps")
public class OpenAppViaRecentTest extends IOSTest {

    private MainPage mainPage;
    private LoginPage loginPage;

    @BeforeTest
    public void setTestData() {
        mainPage = new MainPage();
        loginPage = new LoginPage();
    }

    @Story("Open the app via Recent")
    @Test
    public void openAppViaRecentTest() {
        mainPage.openLoginPage();
        IOSTool.pressHomeButton();
        IOSTool.activateApp();

        assertTrue(loginPage.isOpened(), "LoginPage is not opened after opening the app from Recent apps");
    }
}
