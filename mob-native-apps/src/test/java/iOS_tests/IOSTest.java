package iOS_tests;

import core.MobPlaform;
import core.TLAppiumFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import static core.Platform.IOS;
import static core.TLAppiumServiceFactory.destroyAppiumServices;
import static core.TLDriverFactory.getAppDriver;

public class IOSTest {

    protected static MobPlaform mobPlaform;

    @Parameters({"isReset", "clearAppData"})
    @BeforeSuite
    public void setEnvironment(boolean isReset, boolean clearAppData) {
        mobPlaform = TLAppiumFactory.launchMobPlatform(IOS, isReset, clearAppData);
    }

    @Parameters({"clearAppData"})
    @AfterTest
    public void closeApp(boolean clearAppData) {
        if (clearAppData) {
            getAppDriver().resetApp();
        }
    }

    @AfterSuite
    public void stopAppiumServer() {
        destroyAppiumServices();
    }
}
