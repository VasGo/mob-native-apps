package iOS_tests;

import core.configs.MainConfig;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.android.DocumentsPageAndroid;
import pages.common_pages.LoginPage;
import pages.common_pages.MainPage;
import pages.iOS.RefreshWidgetIOS;
import pages.iOS.TouchIdPopup;
import utils.Direction;
import utils.IOSTool;

import static org.testng.Assert.assertTrue;

@Feature("Pull to refresh")
public class PullToRefreshTest extends IOSTest {

    private MainPage mainPage;
    private LoginPage loginPage;
    private DocumentsPageAndroid documentsPage;
    private TouchIdPopup touchIdPopup;
    private RefreshWidgetIOS refreshWidgetIOS;
    private String email;

    @BeforeTest
    public void setTestData() {
        email = "appstage+1567586825347@gmail.com";
        mainPage = new MainPage();
        loginPage = new LoginPage();
        documentsPage = new DocumentsPageAndroid();
        touchIdPopup = new TouchIdPopup();
        refreshWidgetIOS = new RefreshWidgetIOS();
        documentsPage = new DocumentsPageAndroid();
    }

    @Story("Make pull to refresh")
    @Test
    public void pullToRefreshTest() {
        mainPage.openLoginPage();
        touchIdPopup.closeTouchIdPopup();
        loginPage.login(email, MainConfig.user.password());
        documentsPage.checkUpgradeButtonIsPresent();
        IOSTool.pullToRefresh(Direction.DOWN);

        assertTrue(refreshWidgetIOS.waitForRefreshTextPresent(), "Refresh text with today's date is not appeared after making pull to refresh action");
        assertTrue(refreshWidgetIOS.waitForRefreshTextDisappeared(), "Refresh text with today's date is not disappeared after ending pull to refresh action");
        documentsPage.checkUpgradeButtonIsPresent();
    }
}
