package android_tests.forgot_password;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.android.ForgotPasswordPopupAndroid;
import pages.common_pages.LoginPage;
import pages.common_pages.MainPage;
import android_tests.AndroidTest;

import static org.testng.Assert.assertTrue;

@Feature("Forgot Password")
public class ForgotPasswordViaWrongEmailTest extends AndroidTest {

    private MainPage mainPage;
    private LoginPage loginPage;
    private ForgotPasswordPopupAndroid forgotPasswordPopup;
    private String invalidEmail;

    @BeforeTest
    public void setTestData() {
        invalidEmail = "stageapp@sfsd.d";
        mainPage = new MainPage();
        loginPage = new LoginPage();
        forgotPasswordPopup = new ForgotPasswordPopupAndroid();
    }

    @Story("User can input invalid email to forgot password pop up")
    @Test
    public void inputInvalidEmailTest() {
        mainPage.openLoginPage();
        loginPage.clickForgotPassword();
        forgotPasswordPopup.sendEmail(invalidEmail);

        assertTrue(loginPage.isLabelInvalidEmailPresent(), "Invalid email label is not present after entering the invalid email");
        assertTrue(loginPage.isOpened(), "LoginPage is not opened after entering the wrong email to ForgotPassword pop up");
        assertTrue(loginPage.isLabelInvalidEmailDisappeared(), "Invalid email label is stayed after entering the invalid email");
    }
}
