package android_tests.forgot_password;

import core.configs.MainConfig;
import core.imap.ImapClient;
import core.imap.MessageContent;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.android.ForgotPasswordPopupAndroid;
import pages.common_pages.LoginPage;
import pages.common_pages.MainPage;
import android_tests.AndroidTest;

import javax.mail.MessagingException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Forgot Password")
public class ForgotPasswordViaExistEmailTest extends AndroidTest {

    private MainPage mainPage;
    private LoginPage loginPage;
    private ForgotPasswordPopupAndroid forgotPasswordPopup;
    private ImapClient imap;
    private String subject = "Password Reset Link";
    private String existsEmail;

    @BeforeTest
    public void setTestData() {
        existsEmail = "stageapp+1568642946167@gmail.com";
        mainPage = new MainPage();
        loginPage = new LoginPage();
        forgotPasswordPopup = new ForgotPasswordPopupAndroid();
        imap = new ImapClient(existsEmail, MainConfig.user.password());
        imap.deleteAllMessagesWithSubject(subject);
        imap.setMessagesSearchTime(10);
    }

    @Story("User can input existed email to forgot password pop up")
    @Test
    public void inputExistEmailTest() throws MessagingException {
        mainPage.openLoginPage();
        loginPage.clickForgotPassword();
        forgotPasswordPopup.sendEmail(existsEmail);

        MessageContent content = imap.getLastMessageWithSubject(subject);
        String resetPasswordLink = content.getLink(2); // fetch 2nd ling from mailbox message

        assertTrue(loginPage.isOpened(), "LoginPage is not opened after entering the exist email to ForgotPassword pop up");
        assertTrue(resetPasswordLink.contains("reset-password"), "Reset password link is not present in email message");
        assertEquals(content.getRecipientEmail(), existsEmail, "Reset password link is not delivered to user");
    }
}
