package android_tests;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.android.RecentAppsAndroid;
import pages.common_pages.LoginPage;
import pages.common_pages.MainPage;

import static org.testng.Assert.assertTrue;
import static utils.AndroidTool.openRecentApps;
import static utils.AndroidTool.pressHomeButton;

@Feature("Recent apps")
public class OpenAppViaRecentTest extends AndroidTest {

    private MainPage mainPage;
    private LoginPage loginPage;
    private RecentAppsAndroid recentAppsAndroid;

    @BeforeTest
    public void testData() {
        mainPage = new MainPage();
        loginPage = new LoginPage();
        recentAppsAndroid = new RecentAppsAndroid();
    }

    @Story("Open the app via Recent")
    @Test
    public void openAppViaRecentTest() {
        pressHomeButton();
        openRecentApps();
        recentAppsAndroid.openAppFromRecentApps();
        mainPage.openLoginPage();

        assertTrue(loginPage.isOpened(), "LoginPage is not opened after opening the app from Recent apps");
    }
}
