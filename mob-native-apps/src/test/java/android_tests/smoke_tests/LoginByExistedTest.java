package android_tests.smoke_tests;

import core.configs.MainConfig;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.android.DocumentsPageAndroid;
import pages.common_pages.LoginPage;
import pages.common_pages.MainPage;
import android_tests.AndroidTest;

@Feature("Sign In")
public class LoginByExistedTest extends AndroidTest {

    private MainPage mainPage;
    private LoginPage loginPage;
    private DocumentsPageAndroid documentsPage;
    private String email;

    @BeforeTest
    public void setTestData() {
        email = "appstage+1567586825347@gmail.com";
        mainPage = new MainPage();
        loginPage = new LoginPage();
        documentsPage = new DocumentsPageAndroid();
    }

    @Story("Login by existed user")
    @Test
    public void signInTest() {
        mainPage.openLoginPage();
        loginPage.login(email, MainConfig.user.password());
        documentsPage.checkUpgradeButtonIsPresent();
        documentsPage.checkEmptyFolderIsPresent();
    }
}
