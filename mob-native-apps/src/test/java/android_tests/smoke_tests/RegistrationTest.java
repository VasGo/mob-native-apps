package android_tests.smoke_tests;

import core.configs.MainConfig;
import core.imap.ImapClient;
import core.imap.MessageContent;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.common_pages.LoginPage;
import pages.common_pages.MainPage;
import pages.common_pages.RegistrationPage;
import pages.common_pages.popups.TermsOfServicePopup;
import pages.web_pages.ConfirmationPageAndroid;
import android_tests.AndroidTest;
import utils.StringTool;

import java.io.IOException;

import static org.testng.Assert.assertEquals;

@Feature("Registration")
public class RegistrationTest extends AndroidTest {

    private MainPage mainPage;
    private RegistrationPage registrationPage;
    private LoginPage loginPage;
    private ConfirmationPageAndroid confirmationPage;
    private TermsOfServicePopup termsOfServicePopup;
    private String email;
    private ImapClient imap;
    private String subject = "Email Verification.";

    @BeforeTest
    public void setTestData() {
        email = StringTool.makeUniqueEmail(MainConfig.user.email());
        imap = new ImapClient(email, MainConfig.user.password());
        imap.deleteAllMessagesWithSubject(subject);
        imap.setMessagesSearchTime(10);
        mainPage = new MainPage();
        registrationPage = new RegistrationPage();
        loginPage = new LoginPage();
        confirmationPage = new ConfirmationPageAndroid();
        termsOfServicePopup = new TermsOfServicePopup();
    }

    @Story("Register new user and confirm registration")
    @Test
    public void registrationTest() throws IOException {
        mainPage.openSignUpPage();
        registrationPage.registerUser(email, MainConfig.user.password());
        assertEquals(loginPage.getEmail(), email, "Wrong email after redirection user from registration to login page");

        MessageContent content = imap.getLastMessageWithSubject(subject);
        String verificationLink = content.getLink(2); // // fetch 2nd ling from mailbox message

        mobPlaform.launchBrowserMobile();
        mobPlaform.openUrl(verificationLink);
        confirmationPage.clickIhaveTheApp();
        loginPage.login(email, MainConfig.user.password());

        termsOfServicePopup.checkTermsOfServiceOpened();
    }
}
