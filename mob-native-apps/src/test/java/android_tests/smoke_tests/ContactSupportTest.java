package android_tests.smoke_tests;

import core.configs.MainConfig;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.android.HeaderPanelAndroid;
import pages.common_pages.*;
import pages.android.LeftSideBarAndroid;
import pages.enums.LeftSideBarPage;
import pages.enums.Help;
import pages.android.HelpPopupAndroid;
import pages.common_pages.popups.ThankYouPopup;
import android_tests.AndroidTest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

@Feature("Contact Support")
public class ContactSupportTest extends AndroidTest {

    private MainPage mainPage;
    private LoginPage loginPage;
    private HeaderPanelAndroid headerPanel;
    private HelpPopupAndroid helpPopup;
    private SupportPage supportPage;
    private ThankYouPopup thankYouPopup;
    private LeftSideBarAndroid leftSideBar;
    private String email;
    private String supportText;

    @BeforeTest
    public void setTestData() {
        email = "stageapp+1567586825347@gmail.com";
        supportText = "Test";
        mainPage = new MainPage();
        loginPage = new LoginPage();
        headerPanel = new HeaderPanelAndroid();
        helpPopup = new HelpPopupAndroid();
        supportPage = new SupportPage();
        thankYouPopup = new ThankYouPopup();
        leftSideBar = new LeftSideBarAndroid();
    }

    @Story("Send message to support")
    @Test
    public void sendMessageToSupportTest() {
        mainPage.openLoginPage();
        loginPage.login(email, MainConfig.user.password());
        headerPanel.openLeftSideBar();
        leftSideBar.selectPageFromLeftSideBar(LeftSideBarPage.HELP);
        helpPopup.selectHelpAction(Help.SUPPORT);
        supportPage
                .inputSubject(supportText)
                .inputBody(supportText)
                .sendMessageToSupport();

        assertEquals(thankYouPopup.getTitle(), "Thank You!", "Wrong title in thank you pop up");
        assertEquals(thankYouPopup.getMessage(), "Your message has been sent.", "Wrong message in thank you pop up");
        thankYouPopup.clickOk();
        assertTrue(leftSideBar.isOpened(), "Left sidebar is not opened after clicking OK in 'Thank You pop up'");
        assertFalse(thankYouPopup.isPopupDisplayed(), "'Thank You pop up' is stayed after clicking on button 'OK'");
    }
}
