package android_tests.smoke_tests;

import core.configs.MainConfig;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.android.LeftSideBarAndroid;
import pages.android.DocumentsPageAndroid;
import pages.android.HeaderPanelAndroid;
import pages.common_pages.LoginPage;
import pages.common_pages.MainPage;
import pages.enums.LeftSideBarPage;
import android_tests.AndroidTest;

@Feature("ReLogin")
public class ReLoginTest extends AndroidTest {

    private MainPage mainPage;
    private LoginPage loginPage;
    private DocumentsPageAndroid documentsPage;
    private HeaderPanelAndroid headerPanel;
    private LeftSideBarAndroid leftSideBar;
    private String email;

    @BeforeTest
    public void setTestData() {
        email = "appstage+1567586825347@gmail.com";
        mainPage = new MainPage();
        loginPage = new LoginPage();
        documentsPage = new DocumentsPageAndroid();
        headerPanel = new HeaderPanelAndroid();
        leftSideBar = new LeftSideBarAndroid();
    }

    @Story("User can login after logout")
    @Test
    public void reloginTest() {
        mainPage.openLoginPage();
        loginPage.login(email, MainConfig.user.password());
        headerPanel.openLeftSideBar();
        leftSideBar.selectPageFromLeftSideBar(LeftSideBarPage.LOGOUT);
        mainPage.openLoginPage();
        loginPage.login(email, MainConfig.user.password());

        documentsPage.checkUpgradeButtonIsPresent();
        documentsPage.checkEmptyFolderIsPresent();
    }
}
