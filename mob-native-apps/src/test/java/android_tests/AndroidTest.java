package android_tests;

import core.MobPlaform;
import core.TLAppiumFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import static core.Platform.ANDROID;
import static core.TLAppiumServiceFactory.destroyAppiumServices;
import static core.TLDriverFactory.getAppDriver;

public class AndroidTest {

    protected static MobPlaform mobPlaform;

    @Parameters({"isReset", "clearAppData"})
    @BeforeSuite
    public void setEnvironment(boolean isReset, boolean clearAppData) {
        mobPlaform = TLAppiumFactory.launchMobPlatform(ANDROID, isReset, clearAppData);
    }

    @Parameters({"clearAppData"})
    @AfterTest
    public void closeApp(boolean clearAppData) {
        if (clearAppData) {
            getAppDriver().resetApp();
            getAppDriver().launchApp();
        }
    }

    @AfterSuite
    public void stopAppiumServer() {
        destroyAppiumServices();
    }
}
