package core;

import core.data.AppData;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static core.TLDriverFactory.addStaticDriver;
import static core.TLDriverFactory.appDriver;
import static core.data.AppData.*;

public class AndroidPlatform extends MobPlaform {

    private static Logger logger = LoggerFactory.getLogger(TLAppiumFactory.class);
    private String appPackage;
    private String appActivity;
    private boolean allowTestPackages;

    @Override
    public void setDriverCapabilities() {
        this.automationName = AutomationName.ANDROID_UIAUTOMATOR2;
        this.app = new File(new File(classPathRoot, ANDROID_APP_PATH), ANDROID_APP_NAME);
        this.platformVersion = "5.0";
        this.appPackage = APP_PACKAGE_NAME;
        this.appActivity = AppData.APP_ACTIVITY_NAME;
        this.newCommandTimeout = 300;
        this.allowTestPackages = true;
        this.systemPort = TLAppiumFactory.getCurrentTLScope().driverPort;
        this.udid = TLAppiumFactory.getCurrentTLScope().udid;
    }

    @Override
    public void launchPlatform(boolean isResetApp, boolean clearAppData) {
        Platform platform = TLAppiumFactory.getCurrentTLScope().platform;
        URL appiumServerUrl = TLAppiumServiceFactory.getAppiumUrl();
        AppiumDriver<MobileElement> driver;
        String deviceName = TLAppiumFactory.getCurrentTLScope().deviceName;

        setDriverCapabilities();
        caps = new DesiredCapabilities();

        if (isResetApp) {
            caps.setCapability("fullReset", true);
            caps.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());  //this caps will reinstall app to device
        } else {
            caps.setCapability("fullReset", false);
            caps.setCapability("noReset", true); //Do not stop app, do not clear app data, and do not uninstall apk.
        }
        caps.setCapability("platformName", platform.getName());
        caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, automationName);
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Redmi");
        caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "8.0");
        caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, newCommandTimeout);  // After this time app will close 300 ~ 5 min
        caps.setCapability("autoGrantPermissions", true); //Turn On all permissions
        caps.setCapability("appPackage", appPackage);
        caps.setCapability("appActivity", appActivity);
        caps.setCapability("allowTestPackages", allowTestPackages);
        caps.setCapability("uiautomator2ServerLaunchTimeout", 50000);
        caps.setCapability("systemPort", systemPort); //8201
        caps.setCapability("ignoreUnimportantViews", false);
        caps.setCapability("adbExecTimeout", 50000);

        driver = new AndroidDriver<>(appiumServerUrl, caps);
        if (clearAppData) {
            driver.resetApp();
        }
        addStaticDriver(driver);
        appDriver.set(driver);
        driver.launchApp();
        logger.info("SessionId for Appium Inspector: " + driver.getSessionId().toString());
    }

    @Override
    public void launchBrowserMobile() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        Map<String, Object> options = new HashMap<>();
        options.put("w3c", false);
        caps.setCapability(MobileCapabilityType.BROWSER_NAME, MobileBrowserType.CHROME);
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Redmi");
        caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, automationName);
        caps.setCapability(AndroidMobileCapabilityType.CHROMEDRIVER_EXECUTABLE, new File("src/main/resources/drivers/chromedriver").getAbsolutePath());
        caps.setCapability(AndroidMobileCapabilityType.CHROME_OPTIONS, options);
        AppiumDriver driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
        webDriver.set(driver);
    }

    @Override
    public String toString() {
        return "DriverCapabilities{" +
                "classPathRoot=" + classPathRoot +
                ", automationName='" + automationName + '\'' +
                ", app=" + app +
                ", platformVersion='" + platformVersion + '\'' +
                ", newCommandTimeout=" + newCommandTimeout +
                ", appPackage='" + appPackage + '\'' +
                ", bundleId='" + bundleId + '\'' +
                ", appActivity='" + appActivity + '\'' +
                ", allowTestPackages=" + allowTestPackages +
                ", systemPort=" + systemPort +
                '}';
    }
}