package core.api;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.fluent.Request;
import org.apache.http.util.EntityUtils;
import utils.AllureReport;
import utils.StringTool;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public abstract class HttpBase {

    protected String scheme = "https";
    protected String host;
    private Map<String, String> headers = new HashMap<>();

    public HttpBase(String hostUrl) {
        host = StringTool.removeSchemeFromUrl(hostUrl);
        try {
            if (new URI(hostUrl).getScheme() != null) {
                scheme = new URI(hostUrl).getScheme();
            }
        } catch (URISyntaxException ignored) {
        }
    }

    protected void setHeader(String key, String value) {
        headers.compute(key, (k, v) -> value);
    }

    protected String getResponseBody(HttpResponse resp) throws ParseException, IOException {
        String respBody = EntityUtils.toString(resp.getEntity());
        AllureReport.jsonAttachment(StringTool.jsonPrettyPrint(respBody), "RESPONSE");
        return respBody;
    }

    private Request addHeadersToRequest(Request request) {
        headers.put("user-agent", "pdffiller-autotest");
        headers.forEach((key, value) -> {
            if (key != null && value != null) {
                request.addHeader(key, value);
            }
        });
        AllureReport.jsonAttachment(headers.toString(), "Headers");
        return request;
    }

    protected Request get(URI uri) {
        Request request = Request.Get(uri);
        return addHeadersToRequest(request);
    }

    protected Request post(URI uri) {
        Request request = Request.Post(uri);
        return addHeadersToRequest(request);
    }

    protected Request patch(URI uri) {
        Request request = Request.Patch(uri);
        return addHeadersToRequest(request);
    }

    protected Request put(URI uri) {
        Request request = Request.Put(uri);
        return addHeadersToRequest(request);
    }

    protected Request delete(URI uri) {
        Request request = Request.Delete(uri);
        return addHeadersToRequest(request);
    }

    protected Request head(URI uri) {
        Request request = Request.Head(uri);
        return addHeadersToRequest(request);
    }

    protected boolean isStatusCodeOk(int statusCode) {
        return statusCode >= 200 && statusCode < 300;
    }
}
