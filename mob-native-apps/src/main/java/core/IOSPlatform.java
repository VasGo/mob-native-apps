package core;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import static core.TLDriverFactory.addStaticDriver;
import static core.TLDriverFactory.appDriver;
import static core.data.AppData.*;

public class IOSPlatform extends MobPlaform {

    private String updatedWDABundleId;
    private static Logger logger = LoggerFactory.getLogger(IOSPlatform.class);

    @Override
    public void setDriverCapabilities() {
        this.automationName = "XCUITest";
        this.app = new File(new File(classPathRoot, IOS_APP_PATH), IOS_APP_NAME);
        this.platformVersion = "11.2.5";
        this.bundleId = IOS_BUNDLE_ID;
        this.newCommandTimeout = 300;
        this.updatedWDABundleId = IOS_BUNDLE_ID;
        this.systemPort = TLAppiumFactory.getCurrentTLScope().driverPort;
    }

    @Override
    public void launchPlatform(boolean isResetApp, boolean clearAppData) {
        Platform platform = TLAppiumFactory.getCurrentTLScope().platform;
        URL appiumServerUrl = TLAppiumServiceFactory.getAppiumUrl();
        AppiumDriver<MobileElement> driver;

        setDriverCapabilities();
        caps = new DesiredCapabilities();

        if (isResetApp) {
            caps.setCapability("fullReset", true);
            caps.setCapability("noReset", false);
            caps.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());  //this caps will reinstall app to device
        } else {
            caps.setCapability("fullReset", false);
            caps.setCapability("noReset", true); //Do not stop app, do not clear app data, and do not uninstall apk.
        }
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, platform.getName());
        caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, automationName);
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 6");
        caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.4");
        caps.setCapability(MobileCapabilityType.UDID, "auto");
        caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, newCommandTimeout);  // After this time app will close 300 ~ 5 min
        caps.setCapability("autoGrantPermissions", true); //Turn On all permissions
        caps.setCapability("bundleId", bundleId);
        caps.setCapability("updatedWDABundleId", updatedWDABundleId);
        caps.setCapability("wdaLocalPort", systemPort); //8100
        caps.setCapability("wdaConnectionTimeout", 90000); //Timeout, in ms, for waiting for a response from WebDriverAgent. Defaults to 240000ms.
        caps.setCapability("wdaStartupRetries", 3); //Number of times to try to build and launch WebDriverAgent onto the device. Defaults to 2.
        caps.setCapability("shouldUseSingletonTestManager", false); //Use default proxy for test management within WebDriverAgent. Setting this to false sometimes helps with socket hangup problems. Defaults to true.
        caps.setCapability("xcodeOrgId", XCODE_ORG_ID);

        driver = new IOSDriver<>(appiumServerUrl, caps);
        if (clearAppData) {
            driver.resetApp();
        }
        addStaticDriver(driver);
        appDriver.set(driver);
        logger.info("SessionId for Appium Inspector: " + driver.getSessionId().toString());
    }

    @Override
    public void launchBrowserMobile() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.BROWSER_NAME, MobileBrowserType.SAFARI);
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 6");
        caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.4");
        caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, automationName);
        caps.setCapability(MobileCapabilityType.UDID, "auto");
        caps.setCapability(IOSMobileCapabilityType.START_IWDP, true);
        caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 6000);
        caps.setCapability(MobileCapabilityType.AUTO_WEBVIEW, false);
        AppiumDriver driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
        webDriver.set(driver);
    }

    @Override
    public String toString() {
        return "DriverCapabilities{" +
                "classPathRoot=" + classPathRoot +
                ", automationName='" + automationName + '\'' +
                ", app=" + app +
                ", platformVersion='" + platformVersion + '\'' +
                ", newCommandTimeout=" + newCommandTimeout +
                ", bundleId='" + bundleId + '\'' +
                ", updatedWDABundleId='" + updatedWDABundleId + '\'' +
                ", systemPort=" + systemPort +
                '}';
    }
}
