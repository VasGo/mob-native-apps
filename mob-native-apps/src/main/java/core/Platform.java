package core;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Platform {
    ANDROID("Android"),
    IOS("iOS");

    @Getter
    private String name;
}
