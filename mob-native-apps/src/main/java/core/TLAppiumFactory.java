package core;

import io.qameta.allure.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.AssertionException;
import utils.DeviceTool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class TLAppiumFactory {

    private static Logger logger = LoggerFactory.getLogger(TLAppiumFactory.class);
    private static final ThreadLocal<AppiumParams> threadLocalScope = new ThreadLocal<>();
    private static List<AppiumParams> staticAppiumParams = Collections.synchronizedList(new ArrayList<>());

    private static synchronized void addStaticAppiumParams(AppiumParams params) {
        staticAppiumParams.add(params);
    }

    private static AtomicInteger atomicAppiumPort = new AtomicInteger(4723);
    private static AtomicInteger atomicAndroidSystemPort = new AtomicInteger(8200);
    private static AtomicInteger atomicIosSystemPort = new AtomicInteger(8100);
    private static AtomicInteger atomicIntegerA = new AtomicInteger(0);
    private static AtomicInteger atomicIntegerI = new AtomicInteger(0);

    public static AppiumParams getCurrentTLScope() {
        return threadLocalScope.get();
    }

    private static String getUdid(Platform platformValue) {
        String udid;
        try {
            switch (platformValue) {
                case IOS:
                    int i = atomicIntegerI.getAndIncrement();
                    udid = DeviceTool.getIOSDevices().get(i);
                    break;
                case ANDROID:
                    int j = atomicIntegerA.getAndIncrement();
                    udid = DeviceTool.getAndroidDevices().get(j);
                    break;
                default:
                    udid = null; //TODO
                    break;
            }
        } catch (IndexOutOfBoundsException e) {
            throw new AssertionException("Device is absent in appropriate list!" + "\n" + e.getMessage());
        }
        return udid;
    }

    private static long getDriverPort(Platform platformValues) {
        switch (platformValues) {
            case ANDROID:
                return atomicAndroidSystemPort.getAndIncrement();
            case IOS:
                return atomicIosSystemPort.getAndIncrement();
            default:
                return atomicAndroidSystemPort.getAndIncrement(); //TODO check
        }
    }

    @Step("Set ThreadLocal Appium params")
    public static void setThreadLocalScope(Platform platformValues) {
        //set appiumPort for appropriate Thread:
        int appiumPort = atomicAppiumPort.getAndIncrement();
        //set systemPort for appropriate Thread:
        long systemPort = atomicAndroidSystemPort.getAndIncrement();
        //set driver port:
        long driverPort = getDriverPort(platformValues);
        //set UDID:
        String udid = getUdid(platformValues);
        //set DeviceName to:
        String deviceName = getDeviceName(udid);
        //set Appium Parameters:
        AppiumParams parameters = new AppiumParams(platformValues, deviceName, udid, appiumPort, systemPort, driverPort);

        threadLocalScope.set(parameters);
        addStaticAppiumParams(parameters); //add params to list for clearing after shut down
        logger.warn("Appium_params  =  " + parameters.toString() + " for thread_id = " + Thread.currentThread().getId());
    }

    @Step("Launch platform: {0}, isResetApp: {1}, clearAppData: {2}")
    public static MobPlaform launchMobPlatform(Platform platform, boolean isResetApp, boolean clearAppData) {
        TLAppiumFactory.setThreadLocalScope(platform); //run Appium parameters in thread
        TLAppiumServiceFactory.setTlAppiumService();
        MobPlaform mobPlaform = null;
        switch (platform) {
            case ANDROID:
                mobPlaform = new AndroidPlatform();
                break;
            case IOS:
                mobPlaform = new IOSPlatform();
                break;
            default:
                logger.error("Platform parameters were set incorrectly! Mobile platform cannot be launched");
                break;
        }
        mobPlaform.launchPlatform(isResetApp, clearAppData);

        return mobPlaform;
    }

    private static String getDeviceName(String udid) {
        List<Device> values = Arrays.stream(Device.values()).filter(d -> d.getUdid().equals(udid)).collect(Collectors.toList());
        return values.isEmpty() ? "unidentifiedDevice" : values.get(0).getName();
    }
}