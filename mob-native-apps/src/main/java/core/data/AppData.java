package core.data;

public class AppData {

    public static final String APPIUM_PATH_CI = "/Users/CI/.npm-packages/lib/node_modules/appium/build/lib/main.js";
    public static final String APPIUM_PATH = "/usr/local/lib/node_modules/appium/build/lib/main.js";
    public static final String APPIUM_IP = "0.0.0.0";
    public static final String APP_ACTIVITY_NAME = "com.app.stage.screen_splash.SplashActivity";
    public static final String APP_PACKAGE_NAME = "com.dtage.android";

    public static final String ANDROID_APP_NAME = "app_name-debug.apk";
    public static final String ANDROID_APP_PATH = "src/main/resources/test_files/android_apps";

    public static final String IOS_APP_PATH = "src/main/resources/test_files/iOS_apps/iOS_apps";
    public static final String IOS_APP_NAME = "IOS_APP.ipa";
    public static final String IOS_BUNDLE_ID = "com.app-name.app-name";
    public static final String XCODE_ORG_ID = "ID_NAME";
}
