package core.configs;

import org.aeonbits.owner.ConfigFactory;

public class MainConfig {

    public static User user;

    static {
        user = ConfigFactory.create(User.class);
    }
}
