package core.configs;

import org.aeonbits.owner.Config;

@Config.Sources({
        "file:src/main/resources/user.properties"
})
public interface User extends Config {

    @DefaultValue("appstage@gmail.com")
    String email();

    @DefaultValue("password123")
    String password();
}
