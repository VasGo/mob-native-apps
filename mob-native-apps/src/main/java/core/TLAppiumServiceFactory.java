package core;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.qameta.allure.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.assertTrue;

public class TLAppiumServiceFactory {

    private static Logger logger = LoggerFactory.getLogger(TLAppiumServiceFactory.class);
    private static List<AppiumDriverLocalService> staticServiceList = Collections.synchronizedList(new ArrayList<>());

    private static synchronized void addStaticService(AppiumDriverLocalService service) {
        staticServiceList.add(service);
    }

    private static ThreadLocal<AppiumDriverLocalService> tlAppiumService = new ThreadLocal<>();

    private static AppiumDriverLocalService getTLAppiumService() {
        return tlAppiumService.get();
    }

    public static void setTlAppiumService() {
        //Build the Appium service
        AppiumServiceBuilder appiumServiceBuilder = new AppiumServiceBuilder();
        AppiumDriverLocalService service;
        service = AppiumDriverLocalService.buildService(appiumServiceBuilder);
        if (!service.isRunning()) {
            service.start();
            assertTrue(service.isRunning(), "Appium service is not running! ");
            addStaticService(service); //add to the static list for shutdown
            tlAppiumService.set(service);
            //Set APPIUM URL:
            try {
                TLAppiumFactory.getCurrentTLScope().setAppiumUrl(getAppiumUrl());
            } catch (NullPointerException e) {
                logger.error("Cannot set Appium url");
            }
        }
    }

    private static boolean availablePort(int port) {
        // Assume port is available.
        boolean result = true;

        try {
            new Socket(core.data.AppData.APPIUM_IP, port).close();
            // Successful connection means the port is taken.
            result = false;
        } catch (IOException e) {
            // Could not connect.
        }
        return result;
    }

    private static int getAppiumPort(int appiumPort) {
        if (!availablePort(appiumPort)) {
            appiumPort = appiumPort + 1;
        }
        assertTrue(availablePort(appiumPort), "Appium port is busy!");
        return appiumPort;
    }

    static URL getAppiumUrl() {
        URL appiumUrl = getTLAppiumService().getUrl();
        logger.info("Appium Server_URL = " + appiumUrl);
        return appiumUrl;
    }

    @Step
    static void destroyAllAppiumServices() {
        logger.info("Appium services are destroyed!");
        staticServiceList.forEach(s -> {
            try {
                s.stop();
            } catch (Exception e) {
                logger.warn(e.getMessage());
            }
        });
        staticServiceList.clear();
    }

    public static void destroyAppiumServices() {
        logger.info("Appium services are destroyed!");
        staticServiceList.forEach(s -> {
            try {
                s.stop();
            } catch (Exception e) {
                logger.warn(e.getMessage());
            }
        });
        staticServiceList.clear();
    }
}
