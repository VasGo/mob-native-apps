package core.imap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.search.SearchTerm;

public class With {

    private static Logger logger = LoggerFactory.getLogger(With.class);

    public static SearchTerm subject(final String keyword) {
        logger.info("Subject is: " + keyword);

        return new SearchTerm() {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean match(Message message) {
                try {
                    if (message.getSubject().contains(keyword)) {
                        return true;
                    }
                } catch (MessagingException ex) {
                    logger.error(ex.toString());
                }
                return false;
            }
        };
    }
}
