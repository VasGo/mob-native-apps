package core.imap;

import com.sun.mail.util.MailSSLSocketFactory;
import core.configs.MainConfig;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.TimeTool;

import javax.mail.*;
import javax.mail.search.SearchTerm;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static org.testng.Assert.assertTrue;

public class ImapClient {

    private static Logger logger = LoggerFactory.getLogger(ImapClient.class);
    private static Store store = null;
    private Folder inboxFolder = null;
    private Folder spamFolder = null;

    private String email;
    private String password;
    private String host;

    private int timeout = 2;
    private int retries = 10;

    private static final String NONE = "none";
    private boolean searchInSpam = false;
    private final String OUTLOOK = "imap-mail.outlook.com";
    private final String GMAIL = "imap.gmail.com";
    private final String YAHOO = "imap.mail.yahoo.com";
    private final String MAILRU = "mail.ru";
    private final String YANDEX = "imap.yandex.com";
    private final String AOL = "imap.aol.com";
    private final String APP_STAGE = "mail.app-stage.com.ua";

    public ImapClient(String email, String password) {
        this.email = email.replaceAll("\\+.+@", "@");
        this.password = password;
        if (email.contains("hotmail") || email.contains("outlook")) {
            host = OUTLOOK;
        } else if (email.contains("gmail") || (email.contains("@app-stage.com") || (email.contains("@app-stage.team")))) {
            host = GMAIL;
        } else if (email.contains("yahoo")) {
            host = YAHOO;
        } else if (email.contains("mail.ru")) {
            host = MAILRU;
        } else if (email.contains("yandex")) {
            host = YANDEX;
        } else if (email.contains("aol")) {
            host = AOL;
        } else if (email.contains("@support.app-stage.com")) {
            host = APP_STAGE;
        } else {
            throw new NullPointerException("Unable to determine the host for this email [" + email + "]");
        }
    }

    public void setMessagesSearchTime(int min) {
        this.retries = 10 * min;
    }

    private Store connect() {
        int counter = 0;
        int retries = 10;
        while (isConnected() && counter < retries) {
            Session session;
            if (host.equals("mail.app-stage.com.ua")) {
                final String username = email.replaceAll("@.*", "");
                final String password = MainConfig.user.password();
                Authenticator auth = new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                };

                Properties properties = new Properties();
                MailSSLSocketFactory sf = null;
                try {
                    sf = new MailSSLSocketFactory();
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                }
                assert sf != null;
                sf.setTrustAllHosts(true);
                sf.setTrustedHosts(new String[]{"mail.app-stage.com.ua"});
                properties.put("mail.imap.starttls.enable", "true");
                properties.put("mail.imap.ssl.enable", "true");
                properties.put("mail.imap.ssl.socketFactory", sf);
                properties.put("mail.imap.user", email.replaceAll("@.*", ""));

                session = Session.getInstance(properties, auth);
            } else {
                Properties properties = new Properties();
                session = Session.getInstance(properties);
            }
            try {
                store = session.getStore("imaps");
                store.connect(host, email, password);
                logger.info("Imap connection success");
            } catch (MessagingException e) {
                TimeTool.sleep(timeout);
            }
            counter++;
        }
        return store;
    }

    private boolean isConnected() {
        if (store == null) {
            return true;
        }
        return !store.isConnected();
    }

    private ArrayList<Message> findMessages(String subject) {
        SearchTerm term = With.subject(subject);
        ArrayList<Message> messages = new ArrayList<>();
        int counter = 0;
        assertTrue((connect().isConnected()), "Failed connect to store [" + email + ":" + password + "]");
        while (messages.size() == 0 && counter < retries) {
            if (isConnected()) {
                connect();
            }
            assertTrue(openInboxFolder().isOpen(), "Can't open inbox folder");
            messages.addAll(search(inboxFolder, term));
            if (searchInSpam) {
                assertTrue(openSpamFolder().isOpen(), "Can't open spam folder");
                messages.addAll(search(spamFolder, term));
            }
            TimeTool.sleep(timeout);
            counter++;
        }
        return messages;
    }

    public MessageContent getLastMessageWithSubject(String subject) {
        List<Message> messages = findMessages(subject);
        return new MessageContent(this, messages.get(0));
    }

    private ArrayList<Message> search(Folder folder, SearchTerm term) {
        ArrayList<Message> messages = new ArrayList<>();
        try {
            Message[] folderSearch = folder.search(term);
            messages.addAll(Arrays.asList(folderSearch));
        } catch (MessagingException e) {
            logger.error("Can not search messages");
        }
        return messages;
    }

    public void deleteAllMessagesWithSubject(String subject) {
        logger.info("ImapClient is deleting all messages with subject: " + subject);
        assertTrue(connect().isConnected(), "Failed connect to store [" + email + ":" + password + "]");
        assertTrue(openInboxFolder().isOpen(), "Cant open inbox folder");
        try {
            Message[] messages = inboxFolder.getMessages();
            Flags deleted = new Flags(Flags.Flag.DELETED);
            List<Message> messagesToDelete = new ArrayList<>();
            for (Message message : messages) {
                if (message.getSubject().contains(subject)) {
                    messagesToDelete.add(message);
                }
            }
            if (messagesToDelete.size() > 0) {
                messages = messagesToDelete.toArray(new Message[]{});
                inboxFolder.setFlags(messages, deleted, true);
            }
            inboxFolder.close(true);
            if (searchInSpam) {
                assertTrue(openSpamFolder().isOpen(), "Cant open spam folder");
                Message[] spamMessages = spamFolder.getMessages();
                messagesToDelete = new ArrayList<>();
                for (Message message : spamMessages) {
                    if (message.getSubject().contains(subject)) {
                        messagesToDelete.add(message);
                    }
                }
                if (messagesToDelete.size() > 0) {
                    spamMessages = messagesToDelete.toArray(new Message[]{});
                    spamFolder.setFlags(spamMessages, deleted, true);
                }
                spamFolder.close(true);
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private Folder openInboxFolder() {
        if (inboxFolder == null || !inboxFolder.isOpen()) {
            try {
                inboxFolder = store.getFolder("Inbox");
                inboxFolder.open(Folder.READ_WRITE);
            } catch (MessagingException e) {
                logger.error("Can not open inbox folder");
            }
        }
        return inboxFolder;
    }

    private Folder openSpamFolder() {
        logger.warn("Try to open spam folder");
        if (spamFolder == null || !spamFolder.isOpen()) {
            try {
                spamFolder = store.getFolder(getSpamFolderName());
                spamFolder.open(Folder.READ_WRITE);
            } catch (MessagingException e) {
                try {
                    logger.warn("Can not open spam folder");
                    logger.info("Retrying with [Спам] name");
                    spamFolder = store.getFolder("Спам");
                    spamFolder.open(Folder.READ_WRITE);
                } catch (MessagingException e2) {
                    logger.warn("Can not open spam folder");
                }
            }
        }
        return spamFolder;
    }

    String getContent(Message message) {
        try {
            Object body = message.getContent();
            if (body instanceof Multipart) {
                Multipart multipart = (Multipart) message.getContent();
                int partNumber = 1;
                if (multipart.getCount() == 1) {
                    partNumber = 0;
                }
                BodyPart bodyPart = multipart.getBodyPart(partNumber);
                return IOUtils.toString(bodyPart.getInputStream(), "UTF-8");
            } else {
                return body.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Failed to get content from message");
        }
        return NONE;
    }

    private String getSpamFolderName() {
        if (host.equals("imap-mail.outlook.com")) {
            return "Junk";
        } else {
            if (host.equals("imap.gmail.com")) {
                Folder[] folders;
                try {
                    folders = store.getDefaultFolder().list();
                    for (Folder f : folders) {
                        if (f.getFullName().contains("Spam")) {
                            return "[Gmail]/Spam";
                        } else if ((f.getFullName().contains("Спам"))) {
                            return "[Gmail]/Спам";
                        }
                    }
                } catch (MessagingException e) {
                    logger.error("Can not get spam folder");
                }
                return "[Gmail]/Spam";
            } else {
                if (host.equals("imap.mail.yahoo.com")) {
                    return "Bulk Mail";
                } else {
                    if (host.equals("imap.mail.ru")) {
                        return "Спам";
                    } else {
                        if (host.equals("imap.yandex.com")) {
                            return "Spam";
                        } else {
                            return NONE;
                        }
                    }
                }
            }
        }
    }
}
