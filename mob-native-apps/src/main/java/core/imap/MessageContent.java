package core.imap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MessageContent {

    private String content;
    private Message message;

    public MessageContent(ImapClient imapClient, Message message) {
        this.content = imapClient.getContent(message);
        this.message = message;
    }

    public List<String> getLinkUrls() {
        List<String> links = new ArrayList<>();
        Document doc = Jsoup.parse(content, "UTF-8");
        for (Element link : doc.select("a[href]")) {
            links.add(link.absUrl("href"));
        }
        return links;
    }

    public String getLink(int number) {
        return getLinkUrls().get(number - 1);
    }

    public String getRecipientEmail() throws MessagingException {
        Address[] recipients = message.getAllRecipients();
        return Arrays.stream(recipients).findFirst().get().toString();
    }
}
