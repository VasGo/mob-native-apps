package core;

import java.net.URL;

public class AppiumParams {

    Platform platform;
    private int appiumPort;
    private long systemPort;
    long driverPort;
    public String udid;
    String deviceName;
    private URL appiumUrl;

    AppiumParams(Platform platform, String deviceName, String udid, int appiumPort, long systemPort, long driverPort) {
        this.platform = platform;
        this.deviceName = deviceName;
        this.udid = udid;
        this.systemPort = systemPort;
        this.appiumPort = appiumPort;
        this.driverPort = driverPort;
    }

    URL setAppiumUrl(URL appiumUrl) {
        this.appiumUrl = appiumUrl;
        return appiumUrl;
    }

    @Override
    public String toString() {
        return "AppiumParams{" +
                "platform=" + platform +
                ", appiumPort=" + appiumPort +
                ", systemPort=" + systemPort +
                ", driverPort=" + driverPort +
                ", udid='" + udid + '\'' +
                ", deviceName='" + deviceName + '\'' +
                '}';
    }
}
