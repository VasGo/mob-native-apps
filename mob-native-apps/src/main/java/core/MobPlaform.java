package core;

import io.appium.java_client.AppiumDriver;
import lombok.Setter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;

public abstract class MobPlaform {

    @Setter
    public static ThreadLocal<AppiumDriver<WebElement>> webDriver = new ThreadLocal<>(); // use this driver for web pages on iOS
    protected DesiredCapabilities caps;

    protected String automationName;
    protected File app;
    protected String platformVersion;
    protected long newCommandTimeout;
    protected String bundleId;
    protected long systemPort;
    protected File classPathRoot = new File(System.getProperty("user.dir"));
    protected String udid;

    public static AppiumDriver<WebElement> getWebDriver() {
        return webDriver.get();
    }

    public abstract void setDriverCapabilities();

    public abstract void launchPlatform(boolean isResetApp, boolean clearAppData);

    public abstract void launchBrowserMobile() throws MalformedURLException;

    public void openUrl(String url) {
        webDriver.get().get(url);
    }

    public void closeBrowser() {
        webDriver.get().quit();
    }
}
