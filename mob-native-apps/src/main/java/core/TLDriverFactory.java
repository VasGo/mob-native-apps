package core;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TLDriverFactory {

    public static Object processId;
    public static String logcatFile;
    public static String destLog;

    public static AppiumDriver<MobileElement> getAppDriver() {
        return appDriver.get();
    }

    private static Logger logger = LoggerFactory.getLogger(TLDriverFactory.class);
    static final ThreadLocal<AppiumDriver<MobileElement>> appDriver = new ThreadLocal<>();
    private static ThreadLocal<List<AppiumDriver>> drivers = ThreadLocal.withInitial(ArrayList::new);

    //STATIC ALL THREADS Drivers LIST TO CLOSE ON Shutdown
    private static List<AppiumDriver> staticDrivers = Collections.synchronizedList(new ArrayList<>());

    static synchronized void addStaticDriver(AppiumDriver driver) {
        staticDrivers.add(driver);
    }

    private static List<AppiumDriver> getDriversList() {
        return drivers.get();
    }

    static void quit() {
        getDriversList().forEach(d -> {
            try {
                d.quit();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        });
        getDriversList().clear();
    }
}
