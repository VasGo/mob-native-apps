package core;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Device {
    SAMSUNG("Samsung Galaxy S9+", "12389f24e90d7ece"),
    SAMSUNG10("Samsung Galaxy 10", "RF8M32R9G2C"),
    HUAWEI("Huawei", "udid"),
    IPHONE_6S("iPhone 6s", ""),
    IPHONE_XR("iPhone XR", ""),
    XIAOMI("RedmiNote", "4F465e1f0504"),
    NEXUS("Nexus", "070a0d154h9cb7a");

    private String name;
    private String udid;
}
