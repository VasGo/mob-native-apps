package utils;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.qameta.allure.Step;

import static core.TLDriverFactory.getAppDriver;

public class AndroidTool {

    private static AndroidDriver androidDriver = (AndroidDriver) getAppDriver();

    @Step
    public static void pressHomeButton() {
        androidDriver.pressKey(new KeyEvent(AndroidKey.HOME));
    }

    @Step
    public static void navigateBack() {
        androidDriver.pressKey(new KeyEvent(AndroidKey.BACK));
    }

    @Step
    public static void openRecentApps() {
        androidDriver.pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
    }
}
