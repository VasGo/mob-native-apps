package utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Direction {
    RIGHT("right"),
    LEFT("left"),
    UP("up"),
    DOWN("down");

    @Getter
    private String name;
}
