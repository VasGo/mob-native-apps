package utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Date;

import static org.testng.Assert.fail;

public class StringTool {

    private static Logger logger = LoggerFactory.getLogger(StringTool.class);

    public static String makeUniqueEmail(String email, String addedText) {

        if (email.contains("@")) {
            int atIndex = email.indexOf('@');
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String result = email.substring(0, atIndex) + "+" + ts.getTime() + addedText + email.substring(atIndex);
            logger.info("Unique email: [" + result + "]");
            return result;
        } else {
            return "";
        }
    }

    public static String makeUniqueEmail(String email) {
        if (email.contains("@")) {
            int atIndex = email.indexOf('@');
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String result = email.substring(0, atIndex) + "+" + ts.getTime() + email.substring(atIndex);
            logger.info("Unique email: [" + result + "]");
            return result;
        } else {
            return "";
        }
    }

    public static String removeSchemeFromUrl(String url) {
        String regex = ".*?:/\\/";
        return url.replaceAll(regex, "");
    }

    public static String encodeFileToBase64Binary(String fileName) {
        File file = new File(fileName);
        byte[] encoded = new byte[0];
        try {
            encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
        } catch (IOException e) {
            fail(e.getMessage());
        }
        return new String(encoded, StandardCharsets.US_ASCII);
    }

    public static String jsonPrettyPrint(String json) {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine scriptEngine = manager.getEngineByName("JavaScript");
        scriptEngine.put("jsonString", json);
        try {
            scriptEngine.eval("result = JSON.stringify(JSON.parse(jsonString), null, 2)");
        } catch (ScriptException e) {
            return json;
        }
        return (String) scriptEngine.get("result");
    }
}
