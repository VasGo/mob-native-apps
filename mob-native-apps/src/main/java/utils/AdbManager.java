package utils;

import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdbManager {

    public static org.slf4j.Logger logger = LoggerFactory.getLogger(AndroidDeviceListener.class);
    private String ID;

    AdbManager(String deviceID) {
        ID = deviceID;
    }

    private static String ANDROID_HOME;

    private static String getAndroidHome() {
        if (ANDROID_HOME == null) {
            ANDROID_HOME = System.getenv("ANDROID_HOME");
            if (ANDROID_HOME == null)
                throw new RuntimeException("Failed to find ANDROID_HOME, make sure the environment variable is set!");
        }
        return ANDROID_HOME;
    }

    public static String getDeviceId() {
        String deviceId = "";
        try {
            Process process = Runtime.getRuntime().exec("adb devices");
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;

            Pattern pattern = Pattern.compile("^([a-zA-Z0-9\\-]+)(\\s+)(device)");
            Matcher matcher;

            while ((line = in.readLine()) != null) {
                if (line.matches(pattern.pattern())) {
                    matcher = pattern.matcher(line);
                    if (matcher.find()) {
                        logger.info("Android deviceID = " + matcher.group(1));
                        deviceId = matcher.group(1);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deviceId;
    }

    private static String runCommand(String command) {
        String output = null;
        try {
            Scanner scanner = new Scanner(Runtime.getRuntime().exec(command).getInputStream()).useDelimiter("\\A");
            if (scanner.hasNext()) {
                output = scanner.next();
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return output;
    }

    private static String command(String command) {
        if (command.startsWith("adb")) {
            command = command.replace("adb", getAndroidHome() + "/platform-tools/adb ");
        } else {
            throw new RuntimeException("This method is defed to run AdbManager commands only!");
        }
        logger.info(command);
        String output = runCommand(command);
        if (output == null) {
            return "";
        } else {
            logger.warn(output);
            return output;
        }
    }

    public static void killServer() {
        command("adb kill-server");
    }

    public static void startServer() {
        command("adb start-server");
    }

    public static ArrayList getConnectedDevices() {
        ArrayList devices = new ArrayList();
        String output = command("adb devices");
        for (String line : output.split("\n")) {
            line = line.trim();
            if (line.endsWith("device")) devices.add(line.replace("device", "").trim());
        }
        return devices;
    }

    public String getForegroundActivity() {
        return command("adb -s " + ID + " shell dumpsys window windows | grep mCurrentFocus");
    }

    public String getAndroidVersionAsString() {
        String output = command("adb -s " + ID + " shell getprop ro.build.version.release");
        if (output.length() == 3) output += ".0";
        return output;
    }

    public int getAndroidVersion() {
        return Integer.parseInt(getAndroidVersionAsString().replaceAll("\\.", ""));
    }

    public ArrayList getInstalledPackages() {
        ArrayList packages = new ArrayList();
        String[] output = command("adb -s " + ID + " shell pm list packages").split("\n");
        for (String packageID : output) packages.add(packageID.replace("package:", "").trim());
        return packages;
    }

    public void openAppsActivity(String packageID, String activityID) {
        command("adb -s " + ID + " shell am start -c api.android_tests.intent.category.LAUNCHER -a api.android_tests.intent.action.MAIN -n " + packageID + "/" + activityID);
    }

    public void clearAppsData(String packageID) {
        command("adb -s " + ID + " shell pm clear " + packageID);
    }

    public void forceStopApp(String packageID) {
        command("adb -s " + ID + " shell am force-stop " + packageID);
    }

    public void installApp(String apkPath) {
        command("adb -s " + ID + " install " + apkPath);
    }

    public void uninstallApp(String packageID) {
        command("adb -s " + ID + " uninstall " + packageID);
    }

    public void clearLogBuffer() {
        command("adb -s " + ID + " shell -c");
    }

    public void pushFile(String source, String target) {
        command("adb -s " + ID + " push " + source + " " + target);
    }

    void pullFile(String source, String target) {
        command("adb -s " + ID + " pull " + source + " " + target);
    }

    public void deleteFile(String target) {
        command("adb -s " + ID + " shell rm " + target);
    }

    public void deleteDirectory(String directoryName) {
        command("adb -s " + ID + " shell rm -r sdcard/" + directoryName);
    }

    public void createDirectory(String directoryName) {
        command("adb -s " + ID + " shell mkdir sdcard/" + directoryName);
    }

    public void moveFile(String source, String target) {
        command("adb -s " + ID + " shell mv " + source + " " + target);
    }

    void takeScreenshot(String target) {
        command("adb -s " + ID + " shell screencap " + target);
    }

    public void rebootDevice() {
        command("adb -s " + ID + " reboot");
    }

    public String getDeviceModel() {
        return command("adb -s " + ID + " shell getprop ro.product.model");
    }

    public String getDeviceSerialNumber() {
        return command("adb -s " + ID + " shell getprop ro.serialno");
    }

    public String getDeviceCarrier() {
        return command("adb -s " + ID + " shell getprop gsm.operator.alpha");
    }

    public Object startLogcat(final String logPath) {
        ArrayList pidBefore = getLogcatProcesses();

        //start logcat in Thread:
        Thread logcat = new Thread(() -> command("adb -s " + ID + " shell logcat -v threadtime > " + logPath));

        logcat.setName(logPath);
        logcat.start();
        logcat.interrupt();

        ArrayList pidAfter = getLogcatProcesses();

        for (int i = 0; i < 5; i++) { // need to wait about 5 sec for starting new pid
            TimeTool.sleep(1);
            if (pidBefore.size() > 0) {
                pidAfter.removeAll(pidBefore);
            }
            if (pidAfter.size() > 0) {
                break;
            }
            pidAfter = getLogcatProcesses();
        }

        if (pidAfter.size() == 1) {
            return pidAfter.get(0);
        } else if (pidAfter.size() > 1) {
            throw new RuntimeException("Multiple logcat processes were started when only one was expected!");
        } else {
            throw new RuntimeException("Failed to start logcat process!");
        }
    }

    void stopLocat(Object PID) {
        command("adb -s " + ID + " shell kill " + PID);
    }

    private ArrayList getProcesses(String processName) {
        String[] output = command("adb -s " + ID + " shell top -n 1 | grep -i '" + processName + "'").split("\n");
        ArrayList processes = new ArrayList();
        for (String line : output) {
            Pattern p = Pattern.compile("\\b[0-9]+\\b");
            Matcher m = p.matcher(line);
            if (m.find()) {
                processes.add(m.group(0));
                logger.info(m.group(0));
            }
        }

        return processes;
    }

    private ArrayList getLogcatProcesses() {
        return getProcesses("logcat");
    }

    public Object startRecordingVideo(final String target) {
        logger.info("Start video recording...");
        ArrayList pidBefore = getProcesses("screenrecord");

        Thread recording = new Thread(new Runnable() {
            @Override
            public void run() {
                command("adb -s " + ID + " shell screenrecord " + target); //Video is limited 3 minutes by default
            }
        });

        recording.setName("screenrecord");
        recording.start();
        recording.interrupt();

        ArrayList pidAfter = getProcesses("screenrecord");

        for (int i = 0; i < 5; i++) { // need to wait about 5 sec for starting new pid
            TimeTool.sleep(2);
            if (pidBefore.size() > 0) {
                pidAfter.removeAll(pidBefore);
            }
            if (pidAfter.size() > 0) {
                break;
            }
            pidAfter = getProcesses("screenrecord");
        }

        if (pidAfter.size() == 1) {
            return pidAfter.get(0);
        } else if (pidAfter.size() > 1) {
            throw new RuntimeException("Multiple screenrecord processes were started when only one was expected!");
        } else {
            throw new RuntimeException("Failed to start screenrecord process!");
        }
    }

    public void stopScreenRecord(Object PID) {
        command("adb -s " + ID + " shell kill -2 " + PID);
        TimeTool.sleep(2);
    }

    public void swipe(int startx, int starty, int endx, int endy, int duration) {
        command("adb -s shell input touchscreen swipe " + startx + " " + starty + " " + endx + " " + endy + " " + duration);
    }

    //---------------- iOS commands

    /**
     * @param udid - UDID on real iPhone
     * @throws IOException
     */
    public static void launchWebdriverAgent(String udid) {
        runCommand("cd /");
        runCommand("cd /usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-webDriver/WebDriverAgent");
        runCommand("xcodebuild -project WebDriverAgent.xcodeproj -scheme WebDriverAgentRunner -destination 'id=" + udid + "' test");
    }
}
