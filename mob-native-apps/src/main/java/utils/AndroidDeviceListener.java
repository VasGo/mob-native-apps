package utils;


import core.TLAppiumFactory;
import core.TLDriverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static core.data.AndroidLogsData.DEVICE_STORAGE;
import static core.data.AndroidLogsData.LOGS_DIRECTORY_NAME;

public class AndroidDeviceListener extends TestListenerAdapter {

    public static Logger logger = LoggerFactory.getLogger(AndroidDeviceListener.class);

    private SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
    private SimpleDateFormat time = new SimpleDateFormat("HH-mm-ss");
    private String currentDate = date.format(Calendar.getInstance().getTime());

    private String deviceStoragePath = DEVICE_STORAGE + LOGS_DIRECTORY_NAME + "/" + currentDate;

    @Override
    public void onTestStart(ITestResult result) {
    }

    @Override
    public void onTestSuccess(ITestResult result) {
    }

    @Override
    public void onTestSkipped(ITestResult result) {

    }

    @Override
    public void onFinish(ITestContext result) {

    }

    @Override
    public void onTestFailure(ITestResult result) {
        AdbManager adb = new AdbManager(TLAppiumFactory.getCurrentTLScope().udid);

        String screenshotName = result.getTestClass().getName() + "_" + result.getName()
                + "_" + time.format(Calendar.getInstance().getTime()) + ".png"; // info for name of screenshot

        // take screenshot:
        adb.takeScreenshot(deviceStoragePath + "/" + screenshotName); // it takes screenshot in directory of mobile device
        adb.pullFile(deviceStoragePath + "/" + screenshotName, TLDriverFactory.destLog); // it copies file to our created directory (name of dir=name of dest dir+file name)
        logger.warn("<------- Screeenshot saved to: " + TLDriverFactory.destLog + "/" + screenshotName);
        AllureReport.imageAttachment(screenshotName, TLDriverFactory.destLog + "/" + screenshotName);

        //pull logcat to created directory:
        adb.stopLocat(TLDriverFactory.processId);
        adb.pullFile(deviceStoragePath + "/" + TLDriverFactory.logcatFile, TLDriverFactory.destLog);

        AllureReport.htmlAttachment(TLDriverFactory.logcatFile, TLDriverFactory.destLog + "/" + TLDriverFactory.logcatFile);
        logger.error(result.getTestClass().getName() + " TEST: " + result.getName() + "........FAILED");
    }
}
