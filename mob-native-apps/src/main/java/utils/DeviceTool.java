package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeviceTool {

    private static Logger logger = LoggerFactory.getLogger(DeviceTool.class);

    public static List<String> getAndroidDevices() {

        List<String> deviceIdList = new ArrayList<>();
        String deviceId;
        try {
            Process process = Runtime.getRuntime().exec("adb devices");
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;

            Pattern pattern = Pattern.compile("^([a-zA-Z0-9\\-]+)(\\s+)(device)");
            Matcher matcher;

            while ((line = in.readLine()) != null) {
                if (line.matches(pattern.pattern())) {
                    matcher = pattern.matcher(line);
                    if (matcher.find()) {
                        deviceId = matcher.group(1);
                        logger.info("Android deviceID = " + deviceId);
                        deviceIdList.add(deviceId);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deviceIdList;
    }

    public static List<String> getIOSDevices() {
        List<String> deviceIdList = new ArrayList<>();
        String deviceId;
        try {
            Process process = Runtime.getRuntime().exec("idevice_id -l");
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;

            while ((line = in.readLine()) != null) {
                deviceId = line;
                logger.info("iOS_tests deviceID = " + deviceId);
                deviceIdList.add(deviceId);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deviceIdList;
    }
}
