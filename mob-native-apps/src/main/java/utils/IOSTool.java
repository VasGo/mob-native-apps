package utils;

import com.google.common.collect.ImmutableMap;
import io.appium.java_client.ios.IOSDriver;
import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;

import java.util.HashMap;
import java.util.Map;

import static core.TLDriverFactory.getAppDriver;
import static core.data.AppData.IOS_BUNDLE_ID;

public class IOSTool {

    private static IOSDriver iosDriver = (IOSDriver) getAppDriver();

    public static void launchApp() {
        getAppDriver().launchApp();
    }

    public static void activateApp() {
        //iosDriver.activateApp(IOS_BUNDLE_ID);
        iosDriver.launchApp();
//        JavascriptExecutor js = getAppDriver();
//        js.executeScript("mobile: activateApp", ImmutableMap.of("bundleId", AppData.IOS_BUNDLE_ID));
    }

    public static void terminateApp() {
        Map<String, String> params = new HashMap<>();
        params.put("bundleId", IOS_BUNDLE_ID);
        iosDriver.executeScript("mobile: terminateApp", params);
    }

    @Step
    public static void pressHomeButton() {
        iosDriver.executeScript("mobile: pressButton", ImmutableMap.of("name", "home"));
    }

    @Step
    public static void pullToRefresh(Direction direction) {
        JavascriptExecutor js = iosDriver;
        Map<String, Object> params = new HashMap<>();
        params.put("direction", direction.getName());
        js.executeScript("mobile: swipe", params);
    }
}
