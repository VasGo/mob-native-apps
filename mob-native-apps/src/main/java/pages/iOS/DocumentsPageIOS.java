package pages.iOS;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.qameta.allure.Step;
import pages.BasePage;

public class DocumentsPageIOS extends BasePage {

    public DocumentsPageIOS() {
        super();
    }

    @iOSXCUITFindBy(accessibility = "Upgrade")
    private MobileElement btnUpgrade;

    @iOSXCUITFindBy(accessibility = "Tap the + to add a document")
    private MobileElement btnTapToAddDocument;

    @Step
    public void checkUpgradeButtonIsPresent() {
        waitUntilElementDisplayed(btnUpgrade, 16);
    }

    @Step
    public void checkEmptyFolderIsPresent() {
        waitUntilElementDisplayed(btnTapToAddDocument, 16);
    }
}