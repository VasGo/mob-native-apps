package pages.iOS;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.qameta.allure.Step;
import pages.BasePage;

public class TouchIdPopup extends BasePage {

    public TouchIdPopup() {
        super();
    }

    @iOSXCUITFindBy(accessibility = "Cancel")
    protected MobileElement btnCancel;

    @Step
    public void closeTouchIdPopup() {
        click(btnCancel);
    }
}
