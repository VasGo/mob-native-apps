package pages.iOS;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.qameta.allure.Step;
import pages.BasePage;

public class LogoutPopupIOS extends BasePage {

    public LogoutPopupIOS() {
        super();
    }

    @iOSXCUITFindBy(accessibility = "Yes")
    private MobileElement btnYes;

    @iOSXCUITFindBy(accessibility = "No")
    private MobileElement btnNo;

    @Step("Click Yes")
    public void clickYes() {
        click(btnYes);
    }
}