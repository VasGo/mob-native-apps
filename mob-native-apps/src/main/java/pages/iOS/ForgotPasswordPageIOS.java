package pages.iOS;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.qameta.allure.Step;
import pages.BasePage;

public class ForgotPasswordPageIOS extends BasePage {

    public ForgotPasswordPageIOS() {
        super();
    }

    @iOSXCUITFindBy(xpath = "//*[@value='Enter Email']")
    private MobileElement inputEmail;

    @iOSXCUITFindBy(accessibility = "Renew My Password")
    private MobileElement btnRenewMyPassword;

    @Step("Enter email: {0}")
    public void enterEmail(String email) {
        typeInto(inputEmail, email);
    }

    @Step
    public void clickRenewMyPassword() {
        click(btnRenewMyPassword);
    }

    public boolean isButtonRenewMyPasswordEnabled() {
        return btnRenewMyPassword.isEnabled();
    }

    public boolean isOpened() {
        return isElementDisplayed(btnRenewMyPassword);
    }
}