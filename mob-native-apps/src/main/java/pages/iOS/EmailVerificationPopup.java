package pages.iOS;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.qameta.allure.Step;
import pages.BasePage;

public class EmailVerificationPopup extends BasePage {

    public EmailVerificationPopup() {
        super();
    }

    @iOSXCUITFindBy(accessibility = "OK")
    private MobileElement btnOk;

    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[contains(@name, 'An email has been sent to')]")
    private MobileElement textEmail;

    public String getTextFromEmailVerification() {
        return getText(textEmail);
    }

    @Step
    public void clickOk() {
        click(btnOk);
    }

}
