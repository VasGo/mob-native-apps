package pages.iOS;

import io.appium.java_client.MobileElement;
import io.qameta.allure.Step;
import pages.BasePage;
import pages.enums.Help;

import static core.TLDriverFactory.getAppDriver;

public class HelpPopupIOS extends BasePage {

    public HelpPopupIOS() {
        super();
    }

    @Step("Select Help action: {0}")
    public void selectHelpAction(Help help) {
        MobileElement element = getAppDriver().findElementByAccessibilityId(help.getName());
        click(element);
    }
}