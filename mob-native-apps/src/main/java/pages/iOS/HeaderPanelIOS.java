package pages.iOS;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.qameta.allure.Step;
import pages.BasePage;

public class HeaderPanelIOS extends BasePage {

    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeButton)[1]")
    private MobileElement leftSideBar;

    @Step
    public void openLeftSideBar() {
        click(leftSideBar);
        LeftSideBarIOS sideBar = new LeftSideBarIOS();
        waitUntilElementDisplayed(sideBar.getContent(), 12);
    }
}