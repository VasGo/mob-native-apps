package pages.iOS;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import pages.BasePage;

import java.util.List;

public class RefreshWidgetIOS extends BasePage {

    public RefreshWidgetIOS() {
        super();
    }

    @iOSXCUITFindBy(accessibility = "Release to refresh...")
    private List<MobileElement> textRefresh;

    @iOSXCUITFindBy(accessibility = "Refresh in progress...")
    private MobileElement updatedTextRefresh;

    @iOSXCUITFindBy(accessibility = "Refresh is updating...")
    private MobileElement date;

    private String refreshText = "Refresh in progress...";

    public boolean waitForRefreshTextPresent() {
        return isIosNativeElementPresent(refreshText, 8);
    }

    public boolean waitForRefreshTextDisappeared() {
        return isIosElementDisappeared(refreshText, 8);
    }

    public String getUpdatedDate() {
        return getText(date);
    }
}