package pages.iOS;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.qameta.allure.Step;
import lombok.Getter;
import pages.BasePage;
import pages.enums.LeftSideBarPage;

import static core.TLDriverFactory.getAppDriver;

public class LeftSideBarIOS extends BasePage {

    @Getter
    @iOSXCUITFindBy(accessibility = "Help")
    private MobileElement content;

    @Step("Open page from left SideBar: {0}")
    public void selectPageFromLeftSideBar(LeftSideBarPage type) {
        MobileElement page = getAppDriver().findElementByAccessibilityId(type.getName());
        click(page);
    }

    @Step("Logout")
    public void logout() {
        selectPageFromLeftSideBar(LeftSideBarPage.LOGOUT);
        LogoutPopupIOS logoutPopupIOS = new LogoutPopupIOS();
        logoutPopupIOS.clickYes();
    }

    public boolean isOpened() {
        return isElementDisplayed(content, 8);
    }
}