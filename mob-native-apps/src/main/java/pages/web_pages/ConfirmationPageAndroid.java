package pages.web_pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.qameta.allure.Step;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;

import static core.TLDriverFactory.getAppDriver;

public class ConfirmationPageAndroid extends BasePage {

    public ConfirmationPageAndroid() {
        PageFactory.initElements(new AppiumFieldDecorator(getAppDriver()), this);
    }

    @AndroidFindBy(xpath = "//*[@text='I Have The App']")
    private MobileElement btnIHaveTheApp;

    @Step
    public void clickIhaveTheApp() {
        click(btnIHaveTheApp);
    }
}
