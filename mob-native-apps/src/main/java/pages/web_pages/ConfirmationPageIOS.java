package pages.web_pages;

import core.MobPlaform;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import pages.BasePage;

public class ConfirmationPageIOS extends BasePage {

    @Step
    public void clickIhaveTheApp() {
        By btnIHaveTheApp = By.xpath("//*[@class='btn btn-primary btn-lg btn-block']");
        String accessibilityButtonOpen = "Open";
        waitUntilWebElementDisplayed(btnIHaveTheApp, 30);
        click(MobPlaform.getWebDriver().findElement(btnIHaveTheApp));
        MobPlaform.getWebDriver().context("NATIVE_APP");
        isIosWebElementPresent(accessibilityButtonOpen, 12);
        click(MobPlaform.getWebDriver().findElementByAccessibilityId(accessibilityButtonOpen));
    }
}