package pages.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;
import pages.BasePage;

public class HeaderPanelAndroid extends BasePage {

    @AndroidFindBy(accessibility = "Open navigation drawer")
    private MobileElement leftSideBar;

    @Step
    public void openLeftSideBar() {
        click(leftSideBar);
        LeftSideBarAndroid sideBar = new LeftSideBarAndroid();
        waitUntilElementDisplayed(sideBar.content, 12);
    }
}
