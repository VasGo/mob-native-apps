package pages.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;
import pages.BasePage;
import pages.enums.LeftSideBarPage;

import static core.TLDriverFactory.getAppDriver;
import static io.appium.java_client.touch.offset.PointOption.point;

public class LeftSideBarAndroid extends BasePage {

    @AndroidFindBy(id = "com.stage.android:id/drawerMenuRecyclerView")
    public MobileElement content;

    @Step("Open page from left SideBar: {0}")
    public void selectPageFromLeftSideBar(LeftSideBarPage type) {
        moveLeftSideBarDown();
        MobileElement page = getAppDriver().findElementByXPath("//*[@text='" + type.getName() + "']");
        click(page);
    }

    private void moveLeftSideBarDown() {
        TouchAction action = new TouchAction(getAppDriver());
        int x = 382;
        int y = 946;
        int distanceDown = 200;
        waitUntilElementDisplayed(content, 8);
        action.press(point(x, y)).moveTo(point(x, y - distanceDown)).release().perform();
    }

    public boolean isOpened() {
        return isElementDisplayed(content, 8);
    }
}
