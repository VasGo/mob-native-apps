package pages.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.qameta.allure.Step;
import pages.BasePage;

public class DocumentsPageAndroid extends BasePage {

    public DocumentsPageAndroid() {
        super();
    }

    @AndroidFindBy(id = "com.raise.android:id/button_upgrade")
    @iOSXCUITFindBy(accessibility = "Upgrade")
    private MobileElement btnUpgrade;

    @AndroidFindBy(id = "com.raise.android:id/empty_folder_icon")
    @iOSXCUITFindBy(accessibility = "Tap the + to add a file")
    private MobileElement btnTapToAddDocument;

    @Step
    public void checkUpgradeButtonIsPresent() {
        waitUntilElementDisplayed(btnUpgrade, 16);
    }

    @Step
    public void checkEmptyFolderIsPresent() {
        waitUntilElementDisplayed(btnTapToAddDocument, 16);
    }
}
