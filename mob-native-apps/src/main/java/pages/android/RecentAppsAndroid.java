package pages.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;
import pages.BasePage;

public class RecentAppsAndroid extends BasePage {

    public RecentAppsAndroid() {
        super();
    }

    @AndroidFindBy(id = "com.android.system:id/title")
    private MobileElement appLogo;

    @Step
    public void openAppFromRecentApps() {
        click(appLogo);
    }
}
