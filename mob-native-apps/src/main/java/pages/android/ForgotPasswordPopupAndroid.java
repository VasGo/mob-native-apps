package pages.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;
import pages.BasePage;

public class ForgotPasswordPopupAndroid extends BasePage {

    public ForgotPasswordPopupAndroid() {
        super();
    }

    @AndroidFindBy(id = "com.stage.android:id/et")
    private MobileElement inputEmail;

    @AndroidFindBy(id = "com.stage.android:id/negative_button")
    private MobileElement btnCancel;

    @AndroidFindBy(id = "com.stage.android:id/positive_button")
    private MobileElement btnSend;

    @Step("Send email: {0}")
    public void sendEmail(String email) {
        typeInto(inputEmail, email);
        click(btnSend);
    }

}
