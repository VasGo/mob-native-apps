package pages.android;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import pages.BasePage;
import pages.enums.Help;

import static core.TLDriverFactory.getAppDriver;

public class HelpPopupAndroid extends BasePage {

    public HelpPopupAndroid() {
        super();
    }

    @Step("Select Help action: {0}")
    public void selectHelpAction(Help help) {
        By helpAction = By.xpath("//*[@text='" + help.getName() + "']");
        waitUntilElementDisplayed(helpAction, 12);
        click(getAppDriver().findElement(helpAction));
    }
}