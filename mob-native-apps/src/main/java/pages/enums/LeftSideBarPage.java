package pages.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum LeftSideBarPage {
    DOCUMENTS("Document"),
    ARCHIVE("Archive"),
    ACCOUNT("Account"),
    HELP("Help"),
    LOGOUT("Logout");

    @Getter
    private String name;
}
