package pages.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Help {
    TUTORIALS("Tutorials"),
    SUPPORT("Support"),
    WHATS_NEW("What's new"),
    PRIVACY_POLICY("Privacy policy"),
    TERMS_OF_SERVICE("Terms of service");

    @Getter
    private String name;
}
