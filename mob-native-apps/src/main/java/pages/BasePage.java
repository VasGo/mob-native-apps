package pages;

import core.MobPlaform;
import core.TLDriverFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import utils.TimeTool;

import static core.TLDriverFactory.getAppDriver;
import static java.time.Duration.ofMillis;
import static java.time.Duration.ofSeconds;

public class BasePage {

    private static Logger logger = LoggerFactory.getLogger(TLDriverFactory.class);

    public BasePage() {
        PageFactory.initElements(new AppiumFieldDecorator(getAppDriver()), this);
    }

    public void click(MobileElement element) {
        try {
            WebDriverWait wait = new WebDriverWait(getAppDriver(), 12);
            wait.until(ExpectedConditions.visibilityOf(element));
            element.click();
        } catch (TimeoutException e) {
            logger.error("Cannot click on element: " + element);
            Assert.fail("Cannot click on element: \n" + e.getMessage());
        }
    }

    public void click(WebElement element) {
        try {
            WebDriverWait wait = new WebDriverWait(MobPlaform.getWebDriver(), 12);
            wait.until(ExpectedConditions.visibilityOf(element));
            element.click();
        } catch (TimeoutException e) {
            logger.error("Cannot click on element: " + element);
            Assert.fail("Cannot click on element: \n" + e.getMessage());
        }
    }

    protected void typeInto(MobileElement element, String text) {
        try {
            WebDriverWait wait = new WebDriverWait(getAppDriver(), 30);
            wait.until(ExpectedConditions.visibilityOf(element));
            element.clear();
            element.sendKeys(text);
        } catch (TimeoutException e) {
            logger.error("Cannot typeInto text '" + text + "' into element: " + element);
            Assert.fail("Cannot typeInto text '" + text + "' into element: " + element + "\n" + e.getMessage());
        }
    }

    public void waitUntilElementDisplayed(MobileElement element, int seconds) {
        try {
            Wait wait = new FluentWait(getAppDriver())
                    .withTimeout(ofSeconds(seconds))
                    .pollingEvery(ofMillis(50))
                    .ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class);
            wait.until(ExpectedConditions.visibilityOf(element));
        } catch (TimeoutException e) {
            logger.error("Element is not displayed: " + element);
            Assert.fail("Element is not displayed: " + element + "\n" + e.getMessage());
        }
    }

    public void waitUntilElementDisplayed(By locator, int seconds) {
        try {
            WebDriverWait wait = new WebDriverWait(getAppDriver(), seconds);
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            logger.error("Element is not displayed: " + locator);
            Assert.fail("Element is not displayed: " + locator + "\n" + e.getMessage());
        }
    }

    public void waitUntilWebElementDisplayed(By locator, int seconds) {
        try {
            WebDriverWait wait = new WebDriverWait(MobPlaform.getWebDriver(), seconds);
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (TimeoutException e) {
            logger.error("Element is not displayed: " + locator);
            Assert.fail("Element is not displayed: " + locator + "\n" + e.getMessage());
        }
    }

    protected boolean isElementDisplayed(MobileElement element, int seconds) {
        try {
            WebDriverWait wait = new WebDriverWait(getAppDriver(), seconds);
            wait.until(ExpectedConditions.visibilityOf(element));
            return true;
        } catch (TimeoutException e) {
            logger.error("Element is not displayed: " + element);
            Assert.fail("Element is not displayed: " + element + "\n" + e.getMessage());
            return false;
        }
    }

    protected boolean isElementDisplayed(MobileElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            logger.error("Element is not displayed: " + element);
            return false;
        }
    }

    protected boolean isIosNativeElementPresent(String locator, int seconds) {
        int totalWait = seconds * 100;

        for (int i = 0; i < totalWait; i++) {
            if (getAppDriver().findElementsByAccessibilityId(locator).size() > 0) {
                return true;
            } else {
                TimeTool.sleep(100L);
            }
        }

        return false;
    }

    protected boolean isIosWebElementPresent(String locator, int seconds) {
        int totalWait = seconds * 100;

        for (int i = 0; i < totalWait; i++) {
            if (MobPlaform.getWebDriver().findElementsByAccessibilityId(locator).size() > 0) {
                return true;
            } else {
                TimeTool.sleep(100L);
            }
        }

        return false;
    }

    protected boolean isAndroidNativeElementPresent(String locator, int seconds) {
        int totalWait = seconds * 200;

        for (int i = 0; i < totalWait; i++) {
            if (getAppDriver().findElementsByXPath(locator).size() > 0) {
                return true;
            } else {
                TimeTool.sleep(50L);
            }
        }

        return false;
    }

    protected boolean isIosElementDisappeared(String locator, int seconds) {
        int totalWait = seconds * 100;

        for (int i = 0; i < totalWait; i++) {
            if (getAppDriver().findElementsByAccessibilityId(locator).size() == 0) {
                return true;
            } else {
                TimeTool.sleep(100L);
            }
        }

        return false;
    }

    protected boolean isAndroidElementDisappeared(String locator, int seconds) {
        int totalWait = seconds * 200;

        for (int i = 0; i < totalWait; i++) {
            if (getAppDriver().findElementsByXPath(locator).size() == 0) {
                return true;
            } else {
                TimeTool.sleep(50L);
            }
        }

        return false;
    }

    protected String getText(MobileElement element) {
        try {
            WebDriverWait wait = new WebDriverWait(getAppDriver(), 12);
            wait.until(ExpectedConditions.visibilityOf(element));
            return element.getText();
        } catch (TimeoutException e) {
            logger.error("Cannot get Text from element: " + element);
            Assert.fail("Cannot get Text from element: " + element + "\n" + e.getMessage());
            return null;
        }
    }

    public void switchToAlert(boolean apply) {
        Wait wait = new WebDriverWait(MobPlaform.getWebDriver(), 12);

        try {
            wait.until(ExpectedConditions.alertIsPresent());
            if (apply) {
                MobPlaform.getWebDriver().switchTo().alert().accept();
            } else {
                MobPlaform.getWebDriver().switchTo().alert().dismiss();
            }
        } catch (TimeoutException ex) {
            System.out.println("Alert is not present");
        }
    }
}
