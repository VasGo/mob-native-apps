package pages.common_pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITBy;
import io.appium.java_client.pagefactory.iOSXCUITFindAll;
import io.qameta.allure.Step;
import pages.BasePage;

public class RegistrationPage extends BasePage {

    public RegistrationPage() {
        super();
    }

    @AndroidFindBy(id = "com.stage.android:id/et_email")
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[@type='XCUIElementTypeTextField']"),
            @iOSXCUITBy(accessibility = "key Forgot password")}
    )
    private MobileElement inputEmail;

    @AndroidFindBy(id = "com.stage.android:id/et_pass")
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[@value='Enter your password']"),
            @iOSXCUITBy(xpath = "//*[@type='XCUIElementTypeSecureTextField']")}
    )
    private MobileElement inputPassword;

    @AndroidFindBy(id = "com.stage.android:id/bnt_register")
    @iOSXCUITFindAll({
            @iOSXCUITBy(accessibility = "Continue"),
            @iOSXCUITBy(xpath = "(//XCUIElementTypeButton[@name='Sign Up For Free!'])[2]")}
    )
    private MobileElement btnSignUp;

    @Step("Register user: {0}, {1}")
    public void registerUser(String email, String password) {
        typeInto(inputEmail, email);
        typeInto(inputPassword, password);
        click(btnSignUp);
    }
}
