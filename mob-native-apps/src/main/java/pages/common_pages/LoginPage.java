package pages.common_pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import io.qameta.allure.Step;
import pages.BasePage;

import java.util.List;

public class LoginPage extends BasePage {

    public LoginPage() {
        super();
    }

    @AndroidFindBy(id = "com.stage.android:id/logo")
    private List<MobileElement> logo;

    @AndroidFindBy(id = "com.stage.android:id/email_login")
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[@type='XCUIElementTypeTextField']"),
            @iOSXCUITBy(accessibility = "Enter email")}
    )
    private MobileElement inputEmail;

    @AndroidFindBy(id = "com.stage.android:id/et_pass_login")
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "//*[@value='Enter your password']"),
            @iOSXCUITBy(accessibility = "Enter password")}
    )
    private MobileElement inputPassword;

    @AndroidFindBy(id = "com.stage.android:id/btn_register")
    @iOSXCUITFindAll({
            @iOSXCUITBy(xpath = "(//XCUIElementTypeButton[@name='unlock Login'])[2]"),
            @iOSXCUITBy(accessibility = "Continue")}
    )
    private MobileElement btnLogin;

    @AndroidFindBy(id = "com.stage.android:id/btn_forgot_pass")
    @iOSXCUITFindAll({
            @iOSXCUITBy(accessibility = "Forgot Password?"),
            @iOSXCUITBy(accessibility = "key Forgot password")}
    )
    private MobileElement btnForgotPassword;

    @Step("Login with credentials: {0}, {1}")
    public void login(String email, String password) {
        typeInto(inputEmail, email);
        typeInto(inputPassword, password);
        click(btnLogin);
    }

    @Step("Click Forgot Password")
    public void clickForgotPassword() {
        click(btnForgotPassword);
    }

    public String getEmail() {
        return getText(inputEmail);
    }

    public boolean isOpened() {
        return
                isElementDisplayed(inputEmail, 8) &&
                        isElementDisplayed(inputPassword, 8) &&
                        isElementDisplayed(btnLogin, 8);
    }

    public boolean isLabelInvalidEmailPresent() {
        String locatorInvalidEmail = "//*[@text='Invalid email.']";
        return isAndroidNativeElementPresent(locatorInvalidEmail, 8);
    }

    public boolean isLabelInvalidEmailDisappeared() {
        String locatorInvalidEmail = "//*[@text='Invalid email.']";
        return isAndroidElementDisappeared(locatorInvalidEmail, 8);
    }
}