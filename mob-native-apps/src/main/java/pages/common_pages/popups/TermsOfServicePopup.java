package pages.common_pages.popups;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;
import pages.BasePage;

public class TermsOfServicePopup extends BasePage {

    public TermsOfServicePopup() {
        super();
    }

    @AndroidFindBy(xpath = "//*[@text='Terms of service']")
    private MobileElement textTermsOfService;

    @Step("Check that Terms of Services is opened after Sign In")
    public void checkTermsOfServiceOpened() {
        waitUntilElementDisplayed(textTermsOfService, 12);
    }
}
