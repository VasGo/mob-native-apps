package pages.common_pages.popups;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.qameta.allure.Step;
import pages.BasePage;

public class ThankYouPopup extends BasePage {

    public ThankYouPopup() {
        super();
    }

    @AndroidFindBy(id = "com.stage.android:id/dialog_title")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeStaticText[@name=\"Thank You!\"]")
    private MobileElement title;

    @AndroidFindBy(id = "com.stage.android:id/dialog_message")
    @iOSXCUITFindBy(accessibility = "Your message has been sent.")
    private MobileElement textMessage;

    @AndroidFindBy(id = "com.stage.android:id/positive_button")
    @iOSXCUITFindBy(accessibility = "OK")
    private MobileElement btnOk;

    public String getMessage() {
        return getText(textMessage);
    }

    public String getTitle() {
        return getText(title);
    }

    @Step
    public void clickOk() {
        click(btnOk);
    }

    public boolean isPopupDisplayed() {
        return isElementDisplayed(title) &&
                isElementDisplayed(textMessage) &&
                isElementDisplayed(btnOk);
    }
}
