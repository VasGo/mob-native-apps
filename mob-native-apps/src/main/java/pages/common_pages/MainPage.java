package pages.common_pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import io.qameta.allure.Step;
import pages.BasePage;

import java.util.List;

public class MainPage extends BasePage {

    public MainPage() {
        super();
    }

    @AndroidFindBy(id = "com.stage.android:id/welcome_page_image")
    private List<MobileElement> logo;

    @AndroidFindBy(id = "com.stage.android:id/sign_up_button")
    @iOSXCUITFindAll({
            @iOSXCUITBy(accessibility = "Sign Up"),
            @iOSXCUITBy(accessibility = "pencil Sign Up For Free!")}
    )
    private MobileElement btnSignUpForFree;

    @AndroidFindAll({
            @AndroidBy(id = "com.stage.android:id/btn_onboarding_login"),
            @AndroidBy(id = "com.stage.android:id/login_button")}
    )
    @iOSXCUITFindAll({
            @iOSXCUITBy(accessibility = "unlock Login"),
            @iOSXCUITBy(accessibility = "Log In")}
    )
    private MobileElement btnLogin;

    public boolean isOpened() {
        return logo.size() > 0;
    }

    @Step("Click on button 'SignUpForFree'")
    public void openSignUpPage() {
        click(btnSignUpForFree);
    }

    @Step("Click on button 'Login'")
    public void openLoginPage() {
        click(btnLogin);
    }
}
