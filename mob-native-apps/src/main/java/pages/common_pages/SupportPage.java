package pages.common_pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import io.qameta.allure.Step;
import pages.BasePage;

public class SupportPage extends BasePage {

    public SupportPage() {
        super();
    }

    @AndroidFindBy(id = "com.stage.android:id/ed_subject")
    @iOSXCUITFindBy(accessibility = "(optional)")
    private MobileElement inputSubject;

    @AndroidFindBy(id = "com.stage.android:id/et_problem")
    @iOSXCUITFindBy(accessibility = "Please describe the problem you encountered.")
    private MobileElement inputBody;

    @AndroidFindBy(id = "com.stage.android:id/send")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeNavigationBar[@name=\"Support\"]/XCUIElementTypeButton[2]")
    private MobileElement btnSend;

    @Step("Input Subject into support message: {0}")
    public SupportPage inputSubject(String subject) {
        typeInto(inputSubject, subject);
        return this;
    }

    @Step("Input body into support message: {0}")
    public SupportPage inputBody(String body) {
        typeInto(inputBody, body);
        return this;
    }

    @Step
    public void sendMessageToSupport() {
        click(btnSend);
    }
}
