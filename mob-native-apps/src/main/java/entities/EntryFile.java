package entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class EntryFile {

    private String name;
    private String dateCreated;
    private String email;

}
